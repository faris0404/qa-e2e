import { Selector } from 'testcafe';
import Page from './peluangmodel';
import config from './config';

const page = new Page();

fixture `P2P Pendana-Deposit`
    .page `${config.baseUrl}`

    .httpAuth({
        username: 'morak',
        password: 'morak123',

        
    });

    test('auth', async t => {});

    test('Deposit Dana', async t => {


        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuDeposit)
            .click(page.bank)
            .wait(1000)
            .click(page.ibank)
            
    });