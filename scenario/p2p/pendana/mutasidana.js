import { Selector } from 'testcafe';
import Page from './peluangmodel';
import config from './config';

const page = new Page();

fixture `P2P Pendana-Mutasi`
    .page `${config.baseUrl}`

    .httpAuth({
        username: 'morak',
        password: 'morak123',

        
    });

    test('auth', async t => {});

    test('Mutasi Dana', async t => {


        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuMutasi)
            .click(page.bulanan)
            .click(page.bulanSelect)
            .click(page.bulanOption.withText('Maret'))
            .click(page.tahunSelect)
            .click(page.tahunOption.withText('2019'))
            .click(page.buttonVmutasi)

    });