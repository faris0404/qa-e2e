import { Selector } from 'testcafe';
import Page from './peluangmodel';
import config from './config';

const page = new Page();

fixture `P2P Navigation Peluang`
    .page `${config.baseUrl}`

    .httpAuth({
        username: 'morak',
        password: 'morak123',

        
    });

    test('auth', async t => {});

    test('Referral', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuReferral)
            .expect(Selector('h5').textContent).contains('Referral Anda')

    });

    test('Panduan', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuPanduan)
            .expect(Selector('h1').textContent).contains('Cara Mulai Mendanai ')

    });

    test('Peluang Pendanaan', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuPeluang)
            .expect(Selector('.filter-title').textContent).contains('Tersedia')

    });

    
    test('Cek Inbox', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuInbox)
            .expect(Selector('h3').textContent).contains('Inbox')

    });

    test('Portofolio', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuPortofolio)
            .expect(Selector('h5').textContent).contains('Portofolio Anda')

    });

    test('Mutasi', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuMutasi)
            .expect(Selector('h5').textContent).contains('Periode Mutasi')

    });

    test('Deposit Dana', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuDeposit)
            .expect(Selector('h5').textContent).contains('Deposit Dana')

    });

    test('Tarik Dana', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuTarikDana)
            .expect(Selector('h5').textContent).contains('Penarikan Dana')

    });

    test('Akun', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'harryanto250@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Pendana')
            .click(page.menu)
            .click(page.menuAkun)
            .expect(Selector('h3').textContent).contains('Profil Akun')

    });

    

   
