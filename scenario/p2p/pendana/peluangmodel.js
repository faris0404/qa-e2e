import { Selector } from 'testcafe';

        

export default class Page {
    constructor () {

        //login pendana & peminjam 
        this.emailInput = Selector('#email');
        this.passwordInput = Selector('#password');

        //mendanai
        this.fundingInput = Selector('#funding_amount');
        this.buttonMendanai = Selector('span').withText('Mulai Mendanai');
        this.lihatDetail = Selector('a').withText('Lihat Detil');
        this.clickMendanai = Selector('.btn-primary');
        this.agreement = Selector('.custom-control');
        this.bKonfirmasiPendanaan = Selector('.custom-btn');

        

        //Feature
        this.menu = Selector('span').withText('Menu');
        this.menuReferral = Selector('a').withText('Referral');
        this.menuPanduan = Selector('a').withText('Panduan');
        this.menuPortofolio = Selector('a').withText('Portofolio');
        this.menuInbox = Selector('a').withText('Inbox');
        this.menuPeluang = Selector('a').withText('Peluang Pendanaan');
        this.menuMutasi = Selector('a').withText('Mutasi');
        this.menuDeposit = Selector('a').withText('Deposit Dana');
        this.menuTarikDana = Selector('a').withText('Tarik Dana');
        this.menuAkun = Selector('a').withText('Akun');
        this.menuLogout = Selector('a').withText('Logout');

        //Tarik Dana
        this.inputAmount = Selector('#request_amount');
        this.buttonTarik = Selector('button').withText('Tarik Dana')
        this.buttonKonfirmasi = Selector('button').withText('Konfirmasi')

        //Mutasi Dana
        this.bulanan = Selector('label').withText('Mutasi bulanan')
        this.buttonVmutasi = Selector('button').withText('Lihat')
        this.bulanSelect = Selector('#filterMonth');
        this.bulanOption = this.bulanSelect.find('option');
        this.tahunSelect = Selector('#filterYear');
        this.tahunOption = this.tahunSelect.find('option');

        //Deposit Dana
        this.bank = Selector('#btn-toggler-3');
        this.ibank = Selector('h5').withText('Internet Banking Mandiri');

        
        
        
    }
}