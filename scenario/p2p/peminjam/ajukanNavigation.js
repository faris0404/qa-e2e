import { Selector } from 'testcafe';
import Page from './ajukanmodel';
import config from './config';

const page = new Page();

fixture `P2P Navigation Peminjam`
    .page `${config.baseUrl}`

    .httpAuth({
        username: 'morak',
        password: 'morak123',

        
    });

    test('auth', async t => {});
    
    test('Cek Inbox', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'fariskur0909090@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Peminjam')
            .click(page.menu)
            .click(page.menuInbox)
            .expect(Selector('h3').textContent).contains('Inbox')

    });

    test('Pengajuan Pinjaman', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'fariskur0909090@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Peminjam')
            .click(page.menu)
            .click(page.menuPengajuan)
            .expect(Selector('h1').textContent).contains('Pilih Jenis Pinjaman')

    });

    test('Tagihan', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'fariskur0909090@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Peminjam')
            .click(page.menu)
            .click(page.menuTagihan)
            .expect(Selector('h5').textContent).contains('Daftar Tagihan Anda')

    });

    test('Riwayat', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'fariskur0909090@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Peminjam')
            .click(page.menu)
            .click(page.menuRiwayat)
            .expect(Selector('h5').textContent).contains('Daftar Pinjaman Anda')

    });

    test('Riwayat', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'fariskur0909090@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Peminjam')
            .click(page.menu)
            .click(page.menuMutasi)
            .expect(Selector('h5').textContent).contains('Periode Mutasi')

    });

    test('Akun', async t => {
        await t
            .maximizeWindow()
            //.setTestSpeed(0.1)
            .typeText(page.emailInput,'fariskur0909090@mailinator.com')
            .typeText(page.passwordInput,'morak123')
            .pressKey('enter')
            .expect(Selector('h5').textContent).contains('Peminjam')
            .click(page.menu)
            .click(page.menuAkun)
            .expect(Selector('h3').textContent).contains('Profil Akun')

    });

   
